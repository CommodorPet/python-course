
'''String - Alfabetik Değişkenler'''

doubleQuote = "Bu bir alfabetik ifadedir."
print("Çift Tırnak:",doubleQuote)

singleQuote = 'Bu da bir alfabetik ifadedir.'
print("Tek Tırnak:",singleQuote)

together = "Bugün 'STRING' öğreniyoruz."
print(together)

together = 'Merhaba "COMPET" kanalına hoşgeldiniz!'
print(together)

name = "mutlu"
print(name.upper())

surname = "SU"
print(surname.lower())

fullName = name + " " + surname
print(fullName)

fullName = name.title() + " " + surname.title()
print(fullName)

normal = "WELCOME TO CHANNEL!!"
tab = "\tWELCOME TO CHANNEL!!"
print(normal , "\n", tab, "\n")

channel = " COMPET "
print(channel.lstrip())
print(channel.rstrip())
print(channel.strip())







